#!/usr/bin/env python3
# Version: 1.01

#
# simple UI generator
#
# done with the help of chatgpt, I don't know shit about GUIs ...

# 20240312  malasa 1.0.0 init
#
# gitcommits: 3
# gittag: gitlab-public--xfce4



import sys
import subprocess
import gi
import pyautogui
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Gdk

# Help message
HELP_TEXT = """
Usage: dynamic_ui.py [CONFIG_FILE]

Options:
  -h, --help        Show this help message and exit.

The configuration file can specify options like TITLE, BORDER, MOUSEPOS, and TIMEOUT. Use the following format:
- TITLE=YourWindowTitle
- BORDER=1 (to enable window decorations) or BORDER=0 (to disable)
- MOUSEPOS=1 (to position the window at the mouse cursor) or MOUSEPOS=0 to open centered
- TIMEOUT=number of seconds before auto exit
- HIGHLIGHT_COLOR=#0000FF sets the color, default blue
- To add a button: ButtonLabel,CommandToExecute,ExitAfterExecution
- To add text: TEXT=Your text here
- Use 'NEWLINE' to start a new row of buttons or text.

Examples:
TITLE=My App
BORDER=1
MOUSEPOS=0
TIMEOUT=30
Button1,echo Hello,0
TEXT=This is a label
NEWLINE
Button2,echo Goodbye,1
"""

# Argument parsing logic
if len(sys.argv) == 1 or sys.argv[1] in ['-h', '--help']:
    print(HELP_TEXT)
    sys.exit()
else:
    CONFIG_FILE = sys.argv[1]


class DynamicUI(Gtk.Window):
    def __init__(self, timeout=30):
        super().__init__()
        self.timeout = timeout
        self.init_ui()
        GLib.timeout_add_seconds(self.timeout, self.quit_application)

    def init_ui(self):
        self.connect("key-press-event", self.on_key_press)
        self.connect("focus-out-event", self.on_focus_out)

        self.set_border_width(10)
        title, elements, border, mousepos, timeout, highlight_color = self.parse_config(CONFIG_FILE)
        self.set_title(title)
        self.set_decorated(border)

        if mousepos:
            mouse_x, mouse_y = pyautogui.position()
            self.move(mouse_x, mouse_y)
        else:
            self.set_position(Gtk.WindowPosition.CENTER)

        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.set_row_spacing(10)
        self.add(grid)

        for i, row in enumerate(elements):
            for j, element in enumerate(row):
                if element.startswith('TEXT='):
                    label_text = element.split('=', 1)[1].strip().strip('"')
                    label = Gtk.Label(label=label_text)
                    grid.attach(label, j, i, 1, 1)
                else:
                    parts = element.split(',')
                    if len(parts) >= 3:
                        label = parts[0].strip().strip('"')
                        command = parts[1].strip().strip('"')
                        exit_after = parts[2].strip()
                    elif len(parts) == 2:
                        label = parts[0].strip().strip('"')
                        command = parts[1].strip().strip('"')
                        exit_after = '0'
                    else:
                        continue

                    button = Gtk.Button(label=label)
                    button.connect("clicked", self.on_button_clicked, (command, exit_after))
                    button.set_can_focus(True)
                    grid.attach(button, j, i, 1, 1)

        self.show_all()
        self.apply_css(highlight_color)

    def apply_css(self, highlight_color):
        css_template = """
        button:hover, button:active, button:focus {{
            color: {color}; /* Use the configured highlight color */
            border: 1px solid {color};
        }}
        """
        css = css_template.format(color=highlight_color).encode('utf-8')

        style_provider = Gtk.CssProvider()
        try:
            style_provider.load_from_data(css)
        except Exception as e:
            print(f"Error loading CSS: {e}")
            # This would help identify if there's a formatting issue with the CSS

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

    def parse_config(self, filename):
        title = "no title"
        elements = [[]]
        border = True
        mousepos = False
        timeout = 30  # Default timeout value
        highlight_color = '#0000FF'
        try:
            with open(filename, 'r') as file:
                for line in file:
                    line = line.strip()
                    if line.startswith('#') or line == "":
                        continue
                    if line.startswith('TITLE='):
                        title = line.split('=', 1)[1]
                    elif line.startswith('HIGHLIGHT_COLOR='):
                        highlight_color = line.split('=', 1)[1].strip()
                    elif line.startswith('BORDER='):
                        border = line.split('=', 1)[1] == '1'
                    elif line.startswith('MOUSEPOS='):
                        mousepos = line.split('=', 1)[1] == '1'
                    elif line.startswith('TIMEOUT='):
                        timeout = int(line.split('=', 1)[1])
                    elif line == 'NEWLINE':
                        elements.append([])
                    else:
                        elements[-1].append(line)
        except FileNotFoundError:
            print(f"Configuration file '{filename}' not found.")
            sys.exit(1)
        return title, elements, border, mousepos, timeout, highlight_color

    def on_button_clicked(self, widget, command_exit):
        command, exit_after = command_exit
        subprocess.Popen(command, shell=True)
        if exit_after == '1':
            self.quit_application()

    def on_key_press(self, widget, event):
        if event.keyval == Gdk.KEY_Escape:
            self.quit_application()

    def on_focus_out(self, widget, event):
        self.quit_application()

    def quit_application(self, *args):
        Gtk.main_quit()

if __name__ == "__main__":
    window = DynamicUI()
    window.connect("destroy", Gtk.main_quit)
    window.show_all()
    Gtk.main()
