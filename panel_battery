#!/bin/bash
# myvi 20230303-153140 192.168.1.14 p52
#
# simple generic panel plugin to show battery status for laptops
# when called with $1 status it shows status:
# Batt - running on battery
# AC - charging
# NC - connected but not charging (TLP)
#
# without $1 it shows the battery percentage 
# it would be possible to do it with one panel item, but probably 
# much uglier, less flexible
#
# just add as generic panel item, 5s refresh
#
# 20220612  malasa 1.0.0 init
#
# gitcommits: 3
# gittag: gitlab-public--xfce4

# usually it is BAT0
BAT=BAT0

# script you want to call on click
CALL_CLICK=mybattery_set_tlp

# nothing to change below

if [ -r /sys/class/power_supply/$BAT/capacity ]; then
	CAP_LEVEL="$(cat /sys/class/power_supply/$BAT/capacity)"
fi

if [ "$CAP_LEVEL" = "" ]; then
	CAP_LEVEL="$(upower -i $(upower -e | grep 'BAT') | grep percentage | awk '{print $2}' | sed s,%,, | cut -f1 -d .)"
fi

echo "<txtclick>$CALL_CLICK</txtclick>"

if [ "$1" != "" ] ; then
#	CHARGE_STATUS="`cat /sys/class/power_supply/$BAT/status`"
	CHARGE_STATUS="$(upower -i $(upower -e | grep 'BAT') | grep state: | awk '{print $2}')"

	case $CHARGE_STATUS in
		full ) echo "<txt><span weight='Normal' fgcolor='Green'>Full</span></txt>" ;;
		Discharging | discharging ) echo "<txt><span weight='Normal' fgcolor='Yellow'>Battery</span></txt>" ;;
		"Not charging" | "pending-charge" ) echo "<txt><span weight='Normal' fgcolor='Green'>NC</span></txt><tool>not charging</tool>" ;;
		Unkown ) echo "<txt><span weight='Normal' fgcolor='Green'>AC</span></txt>" ;;
		Charging | charging ) echo "<txt><span weight='Normal' fgcolor='Green'>AC</span></txt>" ;;
		* ) echo "<txt><span weight='Normal' fgcolor='Green'>AC</span></txt>"
	esac   

echo "<bar>$CAP_LEVEL</bar>"

else

	TTE="$(upower -i /org/freedesktop/UPower/devices/battery_${BAT} | grep "time to empty")"
	if [ "$TTE" = "" ]; then
		#CHARGE_STATUS="$(cat /sys/class/power_supply/$BAT/status)"
		CHARGE_STATUS="$(upower -i $(upower -e | grep 'BAT') | grep state: | awk '{print $2}')"
		echo "<tool>"$CHARGE_STATUS"</tool>"
	else
		echo "<tool>"$TTE"</tool>"
	fi

	if [ "$CAP_LEVEL" -gt 97 ]; then
		echo "<txt><span weight='Normal' fgcolor='Green'>full</span></txt>"
		exit
	fi

	if [ "$CAP_LEVEL" -gt 50 ] ; then
		echo "<txt><span weight='Normal' fgcolor='Green'>$CAP_LEVEL</span></txt>"
	elif [ "$CAP_LEVEL" -lt 50 ] ; then
		echo "<txt><span weight='Normal' fgcolor='Yellow'>$CAP_LEVEL</span></txt>"
	elif [ "$CAP_LEVEL" -lt 10 ] ; then
		echo "<txt><span weight='Bold' fgcolor='RED'>$CAP_LEVEL</span></txt>"
	fi
fi

exit 0	
