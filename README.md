# xfce4

Some scripts for xfce4, mostly scripts for the generic panel plugin. I put them on a new panel in "Deskbar" mode.

Note: some functions are probably covered by other panel plugins. Maybe I didn't liked the output, missed some functionality or had a bad day. I know that there might be different ways to archive the same.

| xfce_autorotate | picture |
| :--- | :---: |
|Mostly copied from https://bbs.archlinux.org/viewtopic.php?id=243005 by fishonadish. I just added the locking and use of variables for easier configuration.<br>Rotates screen and touchscreen with xfce or probably any other DE without direct integration,needs iio-sensor-proxy (ubuntu) to be installed and working.<br><br>See also panel item "panel_lock_screen_rotate" to block this script, sometimes you want to stop the auto rotation.<br><br>Just start it when starting desktop, with xfce4 search for "Session and Startup" and add this script | NA, runs in background|

| panel_lock_screen_rotate | picture |
| :--- | :---: |
|Generic panel plugin to lock/unlock screen rotation by clicking on it. Expecting that screenrotation is already working and xfce_autorotate script running. Rather minimal, save space on the Deskbar ... | ![alt text](img/panel_lock_screen_rotate.png "panel lock") | 

| panel_battery | picture |
| :--- | :---: |
|Generic panel plugin to show battery status for laptops. Picture shows 2 genmon items, first with "NC" and bar (when called with option "status") and second one with "78" shows percentage when called without an option. | ![alt text](img/panel_battery.png "panel battery") |

| panel_temp | picture |
| :--- | :---: |
|Generic panel plugin to show CPU temperature, needs sensors package installed/configured. Can display fahrenheit when called with -f | ![alt text](img/panel_temp.png "panel temp") |

| panel_mem | picture |
| :--- | :---: |
|Generic panel plugin to show free memory. Opens terminal or taskmanager on click| ![alt text](img/panel_mem.png "panel mem") |

| mypyui2 | picture |
| :--- | :---: |
|not really only XFCE, a simple UI generator that is more flexible than dialog, see helppage for options| ![alt text](img/mypyui2.png "mymyui2") |
